const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const checkDependencies = require('../node/checkDependencies.js');

function start(entries, htmlTemplate) {
  checkDependencies();
  var config = require('./webpack.default.config.js');
  config.devtool = 'cheap-module-inline-source-map';
  config.entry = config.entry.concat(entries);
  config.plugins.push(
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  );
  config.plugins.push(
    new HtmlWebpackPlugin({
      template: htmlTemplate
    })
  );
  var compiler = webpack(config);
  var server = new WebpackDevServer(compiler, {
    historyApiFallback: true,
    stats: { colors: true }
  });

  server.listen(9000, '0.0.0.0', function() {});
}

module.exports = {
  start
}
