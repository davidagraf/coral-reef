const webpack = require('webpack');
const checkDependencies = require('../node/checkDependencies.js');
const rmDir = require('../node/rmDir.js');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

var NodeEnvironmentProduction = new webpack.DefinePlugin({'process.env.NODE_ENV': '"production"'});

function start(entries, htmlTemplate) {
  checkDependencies();
  rmDir([path.join(process.cwd(), 'dist')]);

  var config = require('./webpack.default.config.js');
  config.entry = config.entry.concat(entries);

  config.plugins.push(
    new HtmlWebpackPlugin({
      template: htmlTemplate
    })
  );

  config.plugins.push(NodeEnvironmentProduction);
  config.plugins.push(new webpack.optimize.UglifyJsPlugin());
  config.plugins.push(new webpack.LoaderOptionsPlugin({minimize: true}));

  var compiler = webpack(config);

  compiler.run(function(err, stats) {
    console.log(stats.hasErrors());
    if (err) {
      console.error(err.stack || err);
      if(err.details) {
        console.error(err.details);
      }
    }

    process.stdout.write(stats.toString({colors :{ level:2, hasBasic:true, has256:true}}) + '\n');

    if (stats.hasErrors()) {
      process.on('exit', function() {
        process.exit(2); // eslint-disable-line
      });
    }
  });
}

module.exports = {
  start
}
