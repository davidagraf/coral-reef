const check = require('check-dependencies');

module.exports = function() {

  check({
    checkGitUrls: true
  }, function(result) {
    // Print the logged errors if dependencies are not ok
    if (!result.depsWereOk && result.error) {
      result.error.forEach(function(message) {
        console.error(message);
      });
    }
    if (result.status) {
      process.exit(result.status);
    }
  });
}
