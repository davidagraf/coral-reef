const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const rootDir = process.cwd();
const os = require('os');
const packageJSON = require(path.join(rootDir, 'package.json'));

function getRevision() {
  try {
    return require('child_process').execSync('git rev-parse HEAD').toString().trim();
  } catch(e) {
    return null;
  }
}

function getBanner() {
  return packageJSON.name +
    os.EOL + 'v' + packageJSON.version + ' rev ' + getRevision() +
    os.EOL + new Date() +
    os.EOL + 'Copyright ' + packageJSON.author;
}

var webpackConfig = {
  context: path.join(rootDir, 'src'),
  entry: ['babel-polyfill'],
  output: {
    path: path.join(rootDir, 'dist'),
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js'
  },
  module: {
    rules: [{
      test: /\.css$/,
      loader: ExtractTextPlugin.extract({
        loader: 'css-loader?sourceMap&-autoprefixer!postcss-loader!resolve-url-loader',
        publicPath: '../'
      })
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        loader: 'css-loader?sourceMap&-autoprefixer' +
                '!postcss-loader!resolve-url-loader!sass-loader?outputStyle=expanded&sourceMap&sourceMapContents',
        publicPath: '../' // set loader specific public path to get assets relative from correct location
      })
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        babelrc: false,
        plugins: ['transform-object-rest-spread'],
        presets: ['es2015']
      }
    }, {
      test: /\.jsx$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        babelrc: false,
        plugins: ['transform-object-rest-spread'],
        presets: ['es2015', 'react']
      }
    }, {
      test: /\.json$/, loader: 'json-loader'
    }, {
      test: /\.(ttf|svg|woff|eot)/,
      loader: 'url-loader?limit=10000&name=assets/fonts/[hash:7].[name].[ext]'
    }, {
      test: /\.(l20n|png|jpg|jpeg)$/,
      loader: 'file-loader?name=assets/images/[hash:7].[name].[ext]'
    }, {
      test: /\.(pdf|docx)$/,
      loader: 'file-loader?name=assets/docs/[path][name].[ext]'
    }, {
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'eslint-loader'
    }]
  },
  plugins: [
    // Extract inline css into separate 'style.css'
    new ExtractTextPlugin('[hash].style.css'),
    new webpack.BannerPlugin(getBanner()),
    new webpack.LoaderOptionsPlugin({
      eslint: {
        configFile: '.eslintrc'
      }
    }),
    // tell moment to load a subset of locales only
    new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|de|fr|it)$/)
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      'node_modules'
    ]
  }
};

module.exports = webpackConfig;
