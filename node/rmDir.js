// Core modules
const fs = require('fs');
const path = require('path');

/**
 * Recursively deletes a folder and all its content
 * @param {string} folder The folder to be deleted
 * @return {void}
 */
function deleteFolderSync(folder) {
  if (fs.existsSync(folder)) {
    fs.readdirSync(folder).forEach(function(fileName) {
      var file = path.join(folder, fileName);
      if (fs.lstatSync(file).isDirectory()) {
        deleteFolderSync(file); // recursive call
      } else {
        fs.unlinkSync(file); // delete file
      }
    });
    fs.rmdirSync(folder); // remove the empty folder
  }
}

function rmDir(dirs) {
  // Remove recursively each directory supplied on the command line
  dirs.forEach(function(folder) {
    deleteFolderSync(folder);
  });
  return 0;
}

module.exports = rmDir;
